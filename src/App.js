import React, { Component } from 'react';
import './App.css';

import Validation from './Validation/Validation';
import Char from './Char/Char';

class App extends Component {
    state = {
        text: ''
    }

    changeLengthHandler = (event) => {
        const text = event.target.value;
        this.setState({ text: text });
    }

    deleteCharHandler = ( charIndex ) => {
        const text =this.state.text.split('');
        text.splice(charIndex, 1);
        const newText = text.join('');
        this.setState({ text: newText });
    }

    render() {

        let chars = null;

        if ( this.state.text !== '' ) {
            const textArray = [...this.state.text];
            chars = (
                <div>
                    {textArray.map( ( char, index ) => {
                        return <Char
                            click={ () => this.deleteCharHandler( index ) }
                            value={ char }
                            key={index}/>
                    })}
                </div>
            );
        }

        return (
          <div className="App">
              <h1>Second Assignment</h1>
              <p>Input text length: {this.state.text.length}</p>
              <input type="text" onChange={this.changeLengthHandler} value={this.state.text}/>

              <Validation textLength={this.state.textLength} />
              {chars}

          </div>
        );
    }
    }

export default App;
