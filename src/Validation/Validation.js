import React from 'react';

const validation = (props) => {
    let message = '';

    if( props.textLength < 5  ) {
        message = 'Text too short';
    }

    if( props.textLength > 15  ) {
        message = 'Text long enough';
    }

    return (
        <div>
            <p>{message}</p>
        </div>
    )
}

export default validation;

